module.exports = {
    dependencies: {},
    url_base2: "http://192.168.1.20:8080",
    url_base: "http://tidemo1.eu01.aws.af.cm",
    url_base3: "http://127.0.0.1:8080",
    uri_country_images: "/icons/country/",
    uri_team_images: "/icons/team/",
    services: {
        list_countries: "/list_countries",
        list_teams: "/list_teams",
        get_team: "/team"
    }
};