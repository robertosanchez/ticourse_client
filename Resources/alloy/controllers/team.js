function Controller() {
    function load_data(teamData) {
        $.team_win.title = teamData.name;
        $.teamWeb.text = teamData.web;
        $.teamStadium.text = teamData.stadium;
        $.teamYear.text = teamData.year;
        $.teamCityCountry.text = _("<%= city %> (<%= country %>)").template({
            city: teamData.city,
            country: countryData.name
        });
        $.countryFlag.image = Alloy.CFG.url_base + Alloy.CFG.uri_country_images + countryData.icon;
        $.teamLogo.image = Alloy.CFG.url_base + Alloy.CFG.uri_team_images + teamData.icon_name;
        console.log("Team logo: " + Alloy.CFG.url_base + Alloy.CFG.uri_team_images + teamData.icon_name);
    }
    function get_team_data() {
        Http.getJSON({
            url: Alloy.CFG.url_base + Alloy.CFG.services.get_team + "/" + teamId,
            success: function(teamData) {
                console.log(JSON.stringify(teamData));
                load_data(teamData);
            },
            fail: function(ev) {
                alert("Error obteniendo datos del equipo.");
            },
            spinner: $.spinner
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    $model = arguments[0] ? arguments[0].$model : null;
    var $ = this, exports = {}, __defers = {};
    $.__views.team_win = A$(Ti.UI.createWindow({
        navBarHidden: !1,
        backgroundColor: "white",
        barColor: "#002b8c",
        id: "team_win"
    }), "Window", null);
    $.addTopLevelView($.__views.team_win);
    $.__views.scrollView = A$(Ti.UI.createScrollView({
        id: "scrollView",
        showVerticalScrollIndicator: "true"
    }), "ScrollView", $.__views.team_win);
    $.__views.team_win.add($.__views.scrollView);
    $.__views.logo_container = A$(Ti.UI.createView({
        top: "10dp",
        width: "100dp",
        height: "100dp",
        id: "logo_container"
    }), "View", $.__views.scrollView);
    $.__views.scrollView.add($.__views.logo_container);
    $.__views.teamLogo = A$(Ti.UI.createImageView({
        width: "auto",
        height: "90dp",
        id: "teamLogo"
    }), "ImageView", $.__views.logo_container);
    $.__views.logo_container.add($.__views.teamLogo);
    $.__views.team_data_container = A$(Ti.UI.createView({
        top: "120dp",
        height: "160dp",
        left: "15dp",
        right: "15dp",
        layout: "vertical",
        borderColor: "#ccc",
        borderRadius: "5dp",
        borderWidth: 1,
        backgroundColor: "#f0f0f0",
        id: "team_data_container"
    }), "View", $.__views.scrollView);
    $.__views.scrollView.add($.__views.team_data_container);
    $.__views.__alloyId0 = A$(Ti.UI.createView({
        width: "100%",
        height: "35dp",
        id: "__alloyId0"
    }), "View", $.__views.team_data_container);
    $.__views.team_data_container.add($.__views.__alloyId0);
    $.__views.teamWebLabel = A$(Ti.UI.createLabel({
        width: "25%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        height: "20dp",
        color: "black",
        left: "10dp",
        text: L("web"),
        id: "teamWebLabel"
    }), "Label", $.__views.__alloyId0);
    $.__views.__alloyId0.add($.__views.teamWebLabel);
    $.__views.teamWeb = A$(Ti.UI.createLabel({
        width: "75%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        font: {
            fontSize: "16dp",
            fontWeight: "bold"
        },
        height: "20dp",
        color: "blue",
        right: "10dp",
        id: "teamWeb"
    }), "Label", $.__views.__alloyId0);
    $.__views.__alloyId0.add($.__views.teamWeb);
    $.__views.__alloyId1 = A$(Ti.UI.createView({
        width: "100%",
        height: "35dp",
        id: "__alloyId1"
    }), "View", $.__views.team_data_container);
    $.__views.team_data_container.add($.__views.__alloyId1);
    $.__views.teamStadiumLabel = A$(Ti.UI.createLabel({
        width: "25%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        height: "20dp",
        color: "black",
        left: "10dp",
        text: L("stadium"),
        id: "teamStadiumLabel"
    }), "Label", $.__views.__alloyId1);
    $.__views.__alloyId1.add($.__views.teamStadiumLabel);
    $.__views.teamStadium = A$(Ti.UI.createLabel({
        width: "75%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        font: {
            fontSize: "16dp",
            fontWeight: "bold"
        },
        height: "20dp",
        color: "black",
        right: "10dp",
        id: "teamStadium"
    }), "Label", $.__views.__alloyId1);
    $.__views.__alloyId1.add($.__views.teamStadium);
    $.__views.__alloyId2 = A$(Ti.UI.createView({
        width: "100%",
        height: "35dp",
        id: "__alloyId2"
    }), "View", $.__views.team_data_container);
    $.__views.team_data_container.add($.__views.__alloyId2);
    $.__views.teamYearLabel = A$(Ti.UI.createLabel({
        width: "25%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        height: "20dp",
        color: "black",
        left: "10dp",
        text: L("year_foundation"),
        id: "teamYearLabel"
    }), "Label", $.__views.__alloyId2);
    $.__views.__alloyId2.add($.__views.teamYearLabel);
    $.__views.teamYear = A$(Ti.UI.createLabel({
        width: "75%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        font: {
            fontSize: "16dp",
            fontWeight: "bold"
        },
        height: "20dp",
        color: "black",
        right: "10dp",
        id: "teamYear"
    }), "Label", $.__views.__alloyId2);
    $.__views.__alloyId2.add($.__views.teamYear);
    $.__views.__alloyId3 = A$(Ti.UI.createView({
        width: "100%",
        height: "35dp",
        id: "__alloyId3"
    }), "View", $.__views.team_data_container);
    $.__views.team_data_container.add($.__views.__alloyId3);
    $.__views.teamCityLabel = A$(Ti.UI.createLabel({
        width: "25%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        height: "20dp",
        color: "black",
        left: "10dp",
        text: L("city"),
        id: "teamCityLabel"
    }), "Label", $.__views.__alloyId3);
    $.__views.__alloyId3.add($.__views.teamCityLabel);
    $.__views.teamCityCountry = A$(Ti.UI.createLabel({
        width: "75%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        font: {
            fontSize: "16dp",
            fontWeight: "bold"
        },
        height: "20dp",
        color: "black",
        right: "40dp",
        id: "teamCityCountry"
    }), "Label", $.__views.__alloyId3);
    $.__views.__alloyId3.add($.__views.teamCityCountry);
    $.__views.country_ico_container = A$(Ti.UI.createView({
        right: "5dp",
        top: "5dp",
        width: "25dp",
        height: "25dp",
        borderRadius: "10dp",
        borderWidth: 1,
        id: "country_ico_container"
    }), "View", $.__views.__alloyId3);
    $.__views.__alloyId3.add($.__views.country_ico_container);
    $.__views.countryFlag = A$(Ti.UI.createImageView({
        id: "countryFlag"
    }), "ImageView", $.__views.country_ico_container);
    $.__views.country_ico_container.add($.__views.countryFlag);
    $.__views.spinner = A$(Ti.UI.createActivityIndicator({
        id: "spinner",
        visible: "false"
    }), "ActivityIndicator", $.__views.team_win);
    $.__views.team_win.add($.__views.spinner);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var data = arguments[0] || {}, animation = require("alloy/animation"), Http = require("common/httpclient"), teamId = data.teamId, countryData = data.country;
    $.team_win.addEventListener("swipe", function(ev) {
        ev.direction === "right" && $.team_win.close();
    });
    $.teamLogo.addEventListener("click", function(e) {
        animation.fadeOut($.teamLogo, 400, function() {
            animation.popIn($.teamLogo);
        });
    });
    $.teamWeb.addEventListener("click", function() {
        Ti.Platform.openURL($.teamWeb.text);
    });
    get_team_data();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A, $model;

module.exports = Controller;