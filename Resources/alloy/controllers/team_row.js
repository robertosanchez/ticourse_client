function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    $model = arguments[0] ? arguments[0].$model : null;
    var $ = this, exports = {}, __defers = {};
    $.__views.team_row = A$(Ti.UI.createTableViewRow({
        height: "60dp",
        id: "team_row"
    }), "TableViewRow", null);
    $.addTopLevelView($.__views.team_row);
    $.__views.ico_team_container = A$(Ti.UI.createView({
        height: "50dp",
        width: "50dp",
        backgroundColor: "#f0f0f0",
        borderRadius: "5dp",
        borderWidth: 1,
        borderColor: "#ccc",
        left: "10dp",
        id: "ico_team_container"
    }), "View", $.__views.team_row);
    $.__views.team_row.add($.__views.ico_team_container);
    $.__views.ico_team = A$(Ti.UI.createImageView({
        height: "45dp",
        width: "auto",
        hires: !1,
        id: "ico_team"
    }), "ImageView", $.__views.ico_team_container);
    $.__views.ico_team_container.add($.__views.ico_team);
    $.__views.team = A$(Ti.UI.createLabel({
        width: "60%",
        height: "20dp",
        left: "70dp",
        color: "black",
        font: {
            fontSize: "18dp",
            fontWeight: "bold"
        },
        ellipsize: !0,
        wordWrap: !1,
        id: "team"
    }), "Label", $.__views.team_row);
    $.__views.team_row.add($.__views.team);
    $.__views.ico_country = A$(Ti.UI.createImageView({
        top: "5dp",
        right: "10dp",
        height: "25dp",
        width: "25dp",
        hires: !1,
        id: "ico_country"
    }), "ImageView", $.__views.team_row);
    $.__views.team_row.add($.__views.ico_country);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var data = arguments[0] || {}, teamData = data.team, countryData = data.country;
    $.team_row._teamId = teamData._id;
    $.team.text = teamData.name;
    $.ico_country.image = Alloy.CFG.url_base + Alloy.CFG.uri_country_images + countryData.icon;
    $.ico_team.image = Alloy.CFG.url_base + Alloy.CFG.uri_team_images + teamData.icon_name;
    console.log("teamData: " + JSON.stringify(teamData));
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A, $model;

module.exports = Controller;