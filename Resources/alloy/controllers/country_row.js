function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    $model = arguments[0] ? arguments[0].$model : null;
    var $ = this, exports = {}, __defers = {};
    $.__views.country_row = A$(Ti.UI.createTableViewRow({
        height: "60dp",
        hasChild: !0,
        id: "country_row"
    }), "TableViewRow", null);
    $.addTopLevelView($.__views.country_row);
    $.__views.ico_container = A$(Ti.UI.createView({
        height: "50dp",
        width: "50dp",
        backgroundColor: "#f0f0f0",
        borderRadius: "10dp",
        borderWidth: 1,
        borderColor: "#ccc",
        left: "10dp",
        id: "ico_container"
    }), "View", $.__views.country_row);
    $.__views.country_row.add($.__views.ico_container);
    $.__views.ico_country = A$(Ti.UI.createImageView({
        height: "45dp",
        width: "auto",
        hires: !1,
        id: "ico_country"
    }), "ImageView", $.__views.ico_container);
    $.__views.ico_container.add($.__views.ico_country);
    $.__views.country = A$(Ti.UI.createLabel({
        width: "60%",
        height: "20dp",
        left: "70dp",
        color: "black",
        font: {
            fontSize: "18dp",
            fontWeight: "bold"
        },
        ellipsize: !0,
        wordWrap: !1,
        id: "country"
    }), "Label", $.__views.country_row);
    $.__views.country_row.add($.__views.country);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var countryData = arguments[0] || {};
    $.country_row.countryData = countryData;
    $.country.text = countryData.name;
    $.ico_country.image = Alloy.CFG.url_base + Alloy.CFG.uri_country_images + countryData.icon;
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A, $model;

module.exports = Controller;