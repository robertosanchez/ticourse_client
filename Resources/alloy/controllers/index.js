function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    $model = arguments[0] ? arguments[0].$model : null;
    var $ = this, exports = {}, __defers = {};
    $.__views.mainTabGroup = A$(Ti.UI.createTabGroup({
        id: "mainTabGroup"
    }), "TabGroup", null);
    $.__views.teams_tab = Alloy.createController("countries", {
        id: "teams_tab"
    });
    $.__views.teams = A$(Ti.UI.createTab({
        title: L("teams"),
        icon: "/tab1.png",
        window: $.__views.teams_tab.getViewEx({
            recurse: !0
        }),
        id: "teams"
    }), "Tab", null);
    $.__views.mainTabGroup.addTab($.__views.teams);
    $.__views.about_tab = Alloy.createController("about", {
        id: "about_tab"
    });
    $.__views.about = A$(Ti.UI.createTab({
        title: L("about"),
        icon: "/tab2.png",
        window: $.__views.about_tab.getViewEx({
            recurse: !0
        }),
        id: "about"
    }), "Tab", null);
    $.__views.mainTabGroup.addTab($.__views.about);
    $.addTopLevelView($.__views.mainTabGroup);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.mainTabGroup.setActiveTab(0);
    $.mainTabGroup.open();
    Alloy.globals.mainTabGroup = $.mainTabGroup;
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A, $model;

module.exports = Controller;