function Controller() {
    function load_data(list_teams) {
        var rows = [];
        _(list_teams).chain().sortBy(function(team) {
            return team.name;
        }).each(function(team) {
            var row = Alloy.createController("team_row", {
                team: team,
                country: countryData
            }).getView();
            rows.push(row);
            console.log("Row: " + JSON.stringify(team));
        });
        $.teamsTable.setData(rows);
    }
    function refresh() {
        Http.getJSON({
            url: Alloy.CFG.url_base + Alloy.CFG.services.list_teams + "?country=" + countryData._id,
            success: function(list_teams) {
                console.log(JSON.stringify(list_teams));
                load_data(list_teams);
            },
            fail: function(ev) {
                alert("Error obteniendo la lista de equipos.");
            },
            spinner: $.spinner
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    $model = arguments[0] ? arguments[0].$model : null;
    var $ = this, exports = {}, __defers = {};
    $.__views.teams_win = A$(Ti.UI.createWindow({
        navBarHidden: !1,
        backgroundColor: "white",
        title: L("teams"),
        barColor: "#002b8c",
        id: "teams_win"
    }), "Window", null);
    $.addTopLevelView($.__views.teams_win);
    $.__views.teamsTable = A$(Ti.UI.createTableView({
        id: "teamsTable"
    }), "TableView", $.__views.teams_win);
    $.__views.teams_win.add($.__views.teamsTable);
    $.__views.spinner = A$(Ti.UI.createActivityIndicator({
        style: Titanium.UI.iPhone.ActivityIndicatorStyle.BIG,
        font: {
            fontSize: "16dp",
            fontWeight: "bold"
        },
        messageid: "loading",
        opacity: 0.8,
        color: "white",
        backgroundColor: "#555",
        width: "180dp",
        height: "60dp",
        borderColor: "#ccc",
        borderRadius: "10dp",
        borderWidth: 1,
        id: "spinner",
        visible: "false"
    }), "ActivityIndicator", $.__views.teams_win);
    $.__views.teams_win.add($.__views.spinner);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var countryData = arguments[0] || {}, Http = require("common/httpclient");
    $.teamsTable.addEventListener("click", function(ev) {
        if (ev.row._teamId) {
            var win = Alloy.createController("team", {
                teamId: ev.row._teamId,
                country: countryData
            }).getView();
            Alloy.globals.mainTabGroup.activeTab.open(win);
        }
    });
    $.teams_win.addEventListener("swipe", function(ev) {
        ev.direction === "right" && $.teams_win.close();
    });
    refresh();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A, $model;

module.exports = Controller;