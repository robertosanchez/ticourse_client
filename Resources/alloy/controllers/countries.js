function Controller() {
    function load_data(list_countries) {
        var rows = [];
        _(list_countries).chain().sortBy(function(country) {
            return country.name;
        }).each(function(country) {
            var row = Alloy.createController("country_row", country).getView();
            rows.push(row);
        });
        $.countriesTable.setData(rows);
    }
    function refresh() {
        Http.getJSON({
            url: Alloy.CFG.url_base + Alloy.CFG.services.list_countries,
            success: function(list_countries) {
                load_data(list_countries);
            },
            fail: function(ev) {
                alert("Error obteniendo la lista de paises.");
            },
            spinner: $.spinner
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    $model = arguments[0] ? arguments[0].$model : null;
    var $ = this, exports = {}, __defers = {};
    $.__views.countries_win = A$(Ti.UI.createWindow({
        navBarHidden: !1,
        backgroundColor: "white",
        title: L("countries"),
        barColor: "#002b8c",
        id: "countries_win"
    }), "Window", null);
    $.addTopLevelView($.__views.countries_win);
    $.__views.countriesTable = A$(Ti.UI.createTableView({
        style: Titanium.UI.iPhone.TableViewStyle.GROUPED,
        id: "countriesTable"
    }), "TableView", $.__views.countries_win);
    $.__views.countries_win.add($.__views.countriesTable);
    $.__views.spinner = A$(Ti.UI.createActivityIndicator({
        font: {
            fontSize: "16dp",
            fontWeight: "bold"
        },
        messageid: "loading",
        opacity: 0.8,
        color: "white",
        backgroundColor: "#555",
        width: "180dp",
        height: "60dp",
        borderColor: "#ccc",
        borderRadius: "10dp",
        borderWidth: 1,
        style: Titanium.UI.iPhone.ActivityIndicatorStyle.BIG,
        id: "spinner",
        visible: "false"
    }), "ActivityIndicator", $.__views.countries_win);
    $.__views.countries_win.add($.__views.spinner);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Http = require("common/httpclient"), refreshButton = Ti.UI.createButton({
        title: L("refresh")
    });
    $.countries_win.rightNavButton = refreshButton;
    refreshButton.addEventListener("click", function(ev) {
        refresh();
    });
    $.countriesTable.addEventListener("click", function(ev) {
        if (ev.row && ev.row.countryData) {
            var win = Alloy.createController("teams", ev.row.countryData).getView();
            Alloy.globals.mainTabGroup.activeTab.open(win);
        }
    });
    refresh();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A, $model;

module.exports = Controller;