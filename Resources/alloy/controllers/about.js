function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    $model = arguments[0] ? arguments[0].$model : null;
    var $ = this, exports = {}, __defers = {};
    $.__views.about_win = A$(Ti.UI.createWindow({
        navBarHidden: !1,
        backgroundColor: "white",
        title: L("about"),
        barColor: "#002b8c",
        id: "about_win"
    }), "Window", null);
    $.addTopLevelView($.__views.about_win);
    $.__views.webview = A$(Ti.UI.createWebView({
        url: "/about/about.html",
        id: "webview"
    }), "WebView", $.__views.about_win);
    $.__views.about_win.add($.__views.webview);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("app:openURL", function(ev) {
        var url = ev.url;
        if (url.indexOf("mailto") === 0) {
            var emailDialog = Titanium.UI.createEmailDialog();
            emailDialog.subject = "iChampions Support. ";
            var mail = url.substring(url.indexOf(":") + 1);
            emailDialog.toRecipients = [ mail ];
            emailDialog.messageBody = "\n\n";
            emailDialog.open();
        } else Ti.Platform.openURL(url);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A, $model;

module.exports = Controller;