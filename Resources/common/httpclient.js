var http = {
    getJSON: function(options) {
        options.spinner && options.spinner.show();
        var client = Ti.Network.createHTTPClient({
            onload: function(e) {
                Ti.API.info(this.location + " Loaded OK ");
                options.spinner && options.spinner.hide();
                var jsonData = JSON.parse(this.responseText);
                options.success && options.success(jsonData);
            },
            onerror: function(e) {
                options.spinner && options.spinner.hide();
                Ti.API.error("HTTP Error (status: " + this.status + ": " + this.statusText + ") message: " + e.error);
                console.error("Error on access to url: " + this.location + " ERROR:" + JSON.stringify(e));
                options.fail && options.fail(e);
            },
            timeout: options.timeout || 5000
        });
        client.open("GET", options.url);
        console.log("options.data: " + JSON.stringify(options.data));
        client.send();
    }
};

module.exports = http;