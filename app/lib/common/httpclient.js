/**
 * Módule to get data from server
 */

var http = {
	/**
	 * 
     * @param {Object} options that acepts next parameters:
     *  - url (mandatory)
     *  - data (optional)
     *  - success (optional) Callback function on success
     *  - fail (optional) Callback function on fail
     *  - spinner (optional) ActivityIndicator to show during call duration
	 */
	getJSON : function(options) {
		if (options.spinner)
			options.spinner.show();
		var client = Ti.Network.createHTTPClient({
			// function called when the response data is available
			onload : function(e) {
				Ti.API.info(this.location + ' Loaded OK ');
				if (options.spinner)
					options.spinner.hide();
				var jsonData = JSON.parse(this.responseText);
				if (options.success)
					options.success(jsonData);
			},
			// function called when an error occurs, including a timeout
			onerror : function(e) {
				if (options.spinner)
					options.spinner.hide();
				Ti.API.error('HTTP Error (status: ' + this.status + ': ' + this.statusText + ') message: ' + e.error);
				console.error('Error on access to url: ' + this.location + " ERROR:" + JSON.stringify(e));
				if (options.fail)
					options.fail(e);
			},
			timeout : options.timeout || 5000 // in milliseconds
		});

		client.open("GET", options.url);
		console.log('options.data: ' + JSON.stringify(options.data));
		client.send();

	}
}

module.exports = http;
