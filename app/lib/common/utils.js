

var Utils = {

	getTableLength: function(table) {
		var list = table.data;
		if (!list || !list.length) return 0;
		return _(list[0].rows).isUndefined() ? list.length : _(list).reduce(function(memo, section){
			return memo + section.rows.length; }, 0);
	}
}

module.exports = Utils