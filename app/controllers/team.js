var data = arguments[0] || {};

var animation = require('alloy/animation');
var Http = require('common/httpclient');

var teamId = data.teamId;
var countryData = data.country;

$.team_win.addEventListener('swipe', function(ev) {
	if (ev.direction === 'right') {
		$.team_win.close();
	} 
});

$.teamLogo.addEventListener('click', function(e) {
	animation.fadeOut($.teamLogo, 400, function() {
		animation.popIn($.teamLogo);
	});
});

$.teamWeb.addEventListener('click', function() {
	Ti.Platform.openURL($.teamWeb.text);
})

function load_data(teamData) {
	$.team_win.title = teamData.name;
	$.teamWeb.text = teamData.web;
	$.teamStadium.text = teamData.stadium;
	$.teamYear.text = teamData.year;
	$.teamCityCountry.text = _('<%= city %> (<%= country %>)').template({
								city: teamData.city,
								country: countryData.name
							});
	
	$.countryFlag.image = Alloy.CFG.url_base + Alloy.CFG.uri_country_images + countryData.icon;
	
	$.teamLogo.image = Alloy.CFG.url_base + Alloy.CFG.uri_team_images + teamData.icon_name;
	console.log('Team logo: ' + Alloy.CFG.url_base + Alloy.CFG.uri_team_images + teamData.icon_name);
}

function get_team_data() {
	Http.getJSON({
		url: Alloy.CFG.url_base + Alloy.CFG.services.get_team + '/' + teamId,
		success: function(teamData) {
			console.log(JSON.stringify(teamData));
			load_data(teamData);
		},
		fail: function(ev) {
			alert('Error obteniendo datos del equipo.');
		},
		spinner: $.spinner
	});
}

get_team_data();
