var countryData = arguments[0] || {};

$.country_row.countryData = countryData;

$.country.text = countryData.name;

$.ico_country.image = Alloy.CFG.url_base + Alloy.CFG.uri_country_images + countryData.icon;

