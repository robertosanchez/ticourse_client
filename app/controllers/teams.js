var countryData = arguments[0] || {};

var Http = require('common/httpclient');

$.teamsTable.addEventListener('click', function(ev) {
	if (ev.row._teamId) {
		var win = Alloy.createController('team', {
			teamId: ev.row._teamId,
			country: countryData
		}).getView();

		Alloy.globals.mainTabGroup.activeTab.open(win);
	} 
});

$.teams_win.addEventListener('swipe', function(ev) {
	if (ev.direction === 'right') {
		$.teams_win.close();
	} 
});

function load_data(list_teams) {
	var rows = [];
	_(list_teams).chain().sortBy(function(team) {
		return team.name;
	}).each(function(team) {
		var row = Alloy.createController('team_row', {
			team: team,
			country: countryData 
		}).getView();
		rows.push(row);
		console.log('Row: ' + JSON.stringify(team));
	});
	$.teamsTable.setData(rows); 
}

function refresh() {
	Http.getJSON({
		url: Alloy.CFG.url_base + Alloy.CFG.services.list_teams + '?country=' + countryData._id,
		success: function(list_teams) {
			console.log(JSON.stringify(list_teams));
			load_data(list_teams);
		},
		fail: function(ev) {
			alert('Error obteniendo la lista de equipos.');
		},
		spinner: $.spinner
	});
}


refresh();
