var data = arguments[0] || {};

var teamData = data.team;
var countryData = data.country;


$.team_row._teamId = teamData._id;

$.team.text = teamData.name;

$.ico_country.image = Alloy.CFG.url_base + Alloy.CFG.uri_country_images + countryData.icon;

$.ico_team.image = Alloy.CFG.url_base + Alloy.CFG.uri_team_images + teamData.icon_name;

console.log('teamData: '+ JSON.stringify(teamData));
