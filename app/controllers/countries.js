
var Http = require('common/httpclient');

var refreshButton = Ti.UI.createButton({
	title: L('refresh')
});

$.countries_win.rightNavButton = refreshButton;

refreshButton.addEventListener('click', function(ev) {
	refresh();
});

$.countriesTable.addEventListener('click', function(ev) {
	if (ev.row && ev.row.countryData) {
		var win = Alloy.createController('teams', ev.row.countryData).getView();

		Alloy.globals.mainTabGroup.activeTab.open(win);
	} 
});

function load_data(list_countries) {
	var rows = [];
	_(list_countries).chain().sortBy(function(country) {
		return country.name;
	}).each(function(country) {
		var row = Alloy.createController('country_row', country).getView();
		rows.push(row);
	});
	$.countriesTable.setData(rows); 
}

function refresh() {
	Http.getJSON({
		url: Alloy.CFG.url_base + Alloy.CFG.services.list_countries,
		success: function(list_countries) {
			load_data(list_countries);
		},
		fail: function(ev) {
			alert('Error obteniendo la lista de paises.');
		},
		spinner: $.spinner
	});
}

// var p2r = Alloy.createController('pull_to_refresh');
// p2r.applyToTableView($.countriesTable, refresh);


refresh();
