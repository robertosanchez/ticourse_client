
Ti.App.addEventListener('app:openURL', function(ev) { 
	var url = ev.url;
	if (url.indexOf('mailto') === 0) {	
		var emailDialog = Titanium.UI.createEmailDialog();
		emailDialog.subject = "iChampions Support. ";
		var mail = url.substring(url.indexOf(':')+1)
		emailDialog.toRecipients = [mail];
		emailDialog.messageBody = '\n\n';
		
		emailDialog.open();
	} else {
    	Ti.Platform.openURL(url);
   }
});